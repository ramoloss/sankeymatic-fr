# SankeyMATIC
## Un générateur de diagrammes de Sankey accessible

Ceci est une traduction de l'interface de SankeyMATIC, originellement disponible à https://github.com/nowthis/sankeymatic

Hébergé et utilisable sur : **https://sankeymatic.noizette.net/**
La version originale : **http://sankeymatic.com/build/**

Basé sur [d3.js](http://d3js.org/) et un [fork de sa bibliothèque Sankey](https://github.com/nowthis/d3-plugin-captain-sankey).

Suivez [@SankeyMATIC](https://twitter.com/SankeyMATIC) sur Twitter pour des nouvelles et mises à jours.

Créé par Steve Bogart, http://nowthis.com/ ([@nowthis](https://twitter.com/nowthis))

